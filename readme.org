#+TITLE: IndieWeb Microservices Project
#+AUTHOR: Vika
#+FILETAGS: pyindieblog
#+STARTUP: lognotedone
This project aims to make another implementation of IndieWeb-enabled blog using microservice architecture.

* Architecture [3/7]
#+BEGIN_SRC ditaa :file architecture.png :cmdline -s 2

      +------------------------+
      |         +---------+    |      +---------+                                   +---------+
      |         | cRED {s}|    |      | cRED {s}|                                   | cRED {s}|
      +-------->+         |    +----->+         |                                   |         |
      |         | Redis DB|           | Redis DB|                                   | Redis DB|
      |         | (tokens)|           | (posts) |                                   | (reader)|
      |         |         |           |         |                                   |         |
      |         +----+----+           +----+----+                                   +----+----+
      |              ^                     ^                                             ^
      |              |                     |                                             |
      |              |                     |                                             |
      |              |                     |                                             |
  +---+---+   +------+-------+      +------+------+           +--------------+           |
  |       |   |              |      | cBLU        |           | cYEL         |           v
  |       |   |              |      |             |           |              |      +----+----+
  | Admin |   |  IndieAuth   |      |  Micropub   |           |   Webmention |      |         |
  |  UI   |   |              +<--=->+             +<--------->+   processor  +----->+ Microsub|
  |       |   |              |      |             |           |              |      |  server |
  |       |   |              |      |             |           |              |      |         |
  +---+-+-+   +------+---+---+      +--+---+------+           +--------------+      +----+----+
      ^ |            ^   ^             ^   |                                             ^
      | |            |   +=------------+---+---=-----------------------------------------+
      | |            |                 |   |
      | +------------+-----------------+   v
      |              |               +-----+-----+
      :              |               | cGRE      |
      |              :               |           |
      |              +-------------->+ Renderer  |
      |                              | front-end |
      +------------------------------+           |
                                     \-----------/
#+END_SRC

#+RESULTS:
[[file:architecture.png]]

The whole services structure is broken down into following services:

** DONE Render frontend
CLOSED: [2019-11-12 Tue 05:09]
:LOGBOOK:
- CLOSING NOTE [2019-11-12 Tue 05:09]
CLOCK: [2019-06-18 Tue 20:59]--[2019-06-18 Tue 21:07] =>  0:08
- I'm starting to slowly implement the Micropub queries.
CLOCK: [2019-06-18 Tue 20:16]--[2019-06-18 Tue 20:31] =>  0:15
- Ok. I finished making it at least extensible enough. Future plans:
   - Allow more configurability:
     + Allow to separate main page and main feed
     + Display h-card on the top or on the bottom
     + Work out how to query [[Micropub]] for configuration while staying compilant to the standards for those who replace the endpoint with something different
CLOCK: [2019-06-18 Tue 19:17]--[2019-06-18 Tue 19:42] =>  0:25
- I'm stuck. The ~theme(template)~ function doesn't work for some reason. This is extremely weird, since it should work. I can't trace the bug and I'm not sure if it's in my code. This is so weird.
CLOCK: [2019-06-18 Tue 18:34]--[2019-06-18 Tue 18:53] =>  0:19
- It starts to work! Although I need to flesh out stuff more.
CLOCK: [2019-06-18 Tue 18:04]--[2019-06-18 Tue 18:29] =>  0:25
CLOCK: [2019-06-18 Tue 16:51]--[2019-06-18 Tue 17:16] =>  0:25
- Ok, I got it. It will need to fetch some information from the backend about the blog, since the renderer is supposed to be stateless. Also, I started to write the first theme - plain! It contains almost to no CSS and serves as a starting point for other themes.
CLOCK: [2019-06-18 Tue 16:21]--[2019-06-18 Tue 16:46] =>  0:25
- Theme settings page becomes richer. I really need to implement my templates though...
CLOCK: [2019-06-18 Tue 15:50]--[2019-06-18 Tue 16:15] =>  0:25
- I'm continuing to implement the frontend. Currently it has prototypes for most functions. I'll need to add requests module for accessing the Micropub and some basic templates.
CLOCK: [2019-06-18 Tue 15:12]--[2019-06-18 Tue 15:37] =>  0:25
- I've started writing a renderer frontend. Flask-Theme extension provides theming support. Maybe support several themes depending on user? I've never seen this before!
:END:

Renders entries (optionally into static files for webservers) from database, allowing readers to receive entries.

As h-entry format is the default, no conversion for listing feeds is required. It will render posts received from database (posted there with micropub) and comments (gathered by the webmention endpoint into the very same database). Can be read-only, because it doesn't modify the database.

IndieWeb blogs are often static, save for the Micropub and webmention endpoints. Maybe the render will be able to output static files?

When a Micropub receives and writes a post, it could ping the system through Redis pubsub framework. That will allow static renderers to pick up updates and re-render the pages accordingly.

** DONE Micropub
CLOSED: [2019-06-18 Tue 20:40]
- CLOSING NOTE [2019-06-18 Tue 20:40] \\
  The micropub endpoint basics are finished. What's left is extensions to it - some are even specific to my exact software and are not part of any standard.
Receives requests to write and update entries in the database. Obviously this needs write access.

*** Extensions
**** Required
A lot of extensions will be required for Micropub service to work as a full storage database:
 - [[https://github.com/indieweb/micropub-extensions/issues/4][Query for Post List]] :: allows to get a list of posts (potentially satisfying certain conditions) in a paginated form. Useful not only for whipping up a list of posts to edit, but for rendering these posts.
**** Additional [0%]
***** TODO [[https://github.com/indieweb/micropub-extensions/issues/5][Query for Category/Tag List]]
Autocompletion for tags is a nice thing.
***** TODO [[https://github.com/indieweb/micropub-extensions/issues/19][Post Status]]
I think it should be =draft=, =published=, =deleted=
***** TODO [[https://github.com/indieweb/micropub-extensions/issues/11][Visibility]]
For private posts
***** TODO [[https://github.com/indieweb/micropub-extensions/issues/12][Audience]]
So someone would read all of these private posts...

** DONE Media endpoint
CLOSED: [2019-11-12 Tue 05:09]
:LOGBOOK:
- CLOSING NOTE [2019-11-12 Tue 05:09] \\
  I used @martymcguire's Spano to have a media endpoint.
:END:
Receives attachments for posts and saves them in a readily-accessed static folder.

** TODO Webmention processor
Receives webmentions and writes them to the database. This updates the posts via Micropub.

** TODO Microsub service
Records blogs the user is subscribed to. Needs write access to its own portion of the database, not shared with anything else.
*** Web Push?
#+BEGIN_EXAMPLE
POST /microsub HTTP/1.1
Content-type: application/json

<JSON>
#+END_EXAMPLE
** TODO IndieAuth and token endpoints
A Single Sign-On solution for the indie web. Can be stateless or stateful. State allows to revoke tokens generated by keeping a list of valid tokens.

** TODO Administration frontend
An interface both to Micropub API and IndieAuth token endpoint. Allows for posting and changing settings. Will require A LOT of JavaScript.

* Data and state storage
** Format for posts
IndieWeb mandates h-entry formatting for posts, so it will be logical to store posts in something resembling h-entry JSON representation. ReJSON may be useful for storing posts if we're going with Redis.
** Database
The most logical solution is to use a NoSQL database, since there won't be any reason to use a SQL database - this produces an unnecessary complexity and slows down stuff (everyone knows HOW SLOW a MariaDB on Raspberry Pi can be!)

The database should be accessed by network and the support for extended data types (like JSON data) may be beneficial. Redis seems like a nice candidate.

*** Redis database structure by key timeouts
Redis DB structure could be separated into cache keys, persistent keys and configuration keys.

**** Persistent keys
These keys correspond to persistent state of the blog. Loss of data from these is catastrophic.
 - ~posts~ :: A hash of posts by id. Written by micropub, handled by renderer. Comments are embedded within each h-entry via an internal field.
 - ~slugs~ :: A hash of post slugs for redirections to occur. Read by renderer, written sometimes by Micropub.
 - ~tokens~ :: A hash of tokens and JSON-ified access properties. Written and read by IndieAuth module. Interestingly, doing it as a hash allows for quick token checking, the token operation would be EXTREMELY quick!
**** Cache keys
These are easily recreated and are here for speeding up the site.
 - ~posts_order~ :: A list of post IDs in the order they should appear in the news - i.e. sorted by the post date. This assists with paging and lowers network traffic needed to render pages.
 - ~categories~ :: A set of tags used for posts. Updated by Micropub and used in queries for autocompletion.

**** Configuration keys
I mean, why not store the configuration IN THE DATABASE itself? This feels useful because the config on-disk will only consist of DB URL and password. These could be even dumped in the environment (heroku deployment for blog?)

The following keys are persistent, but their loss is not THAT catastrophic. Nothing would work without them, but they are recreated.
 - ~password~ :: A password sha256(?) digest for IndieAuth with its salt. Checked by IndieAuth module to allow access to administrative interfaces and verify a user in IndieAuth auth endpoint flow.
 - ~timezone~ :: Used for time adjustment for ISO 8601 timestamps. Used by Micropub for writing posts.

*** Redis DB structure by DB users
**** Micropub
 - ~posts~ :: A hash of posts by ID (URL?). Uses JSON-ified strings to store arbitrary MF2 data.
 - ~posts_order~ :: Main feed list for easier paging.
 - ~categories~ :: Cache for hashtags.
 - ~timezone~ :: Time adjustment for ISO 8601 timestamps. Can be changed if you switch to another timezone :3 Old posts will stay in the previous timezone.
 - ~addressbook~ :: A collection of h-cards for every person and venue I've met or visited.
 - ~site_settings~ :: A JSON dictionary of stuff needed for renderer to function.
**** IndieAuth
 - ~password~ :: A digest of the password for identity checking.
 - ~tokens~ :: A hash of tokens and their JSON-ified access properties.

**** Renderer
Renderer uses no keys and is not configured aside from endpoint URLs.

**** TODO Microsub
**** Webmention
Webmention endpoint sends CRUD requests to Micropub and doesn't need any of this stuff...

*** Useful links about Redis
- [[https://stackoverflow.com/questions/5780365/redis-how-can-i-sort-my-hash-by-keys][Sorting hashes by keys]]

** Blobs
Sometimes an image is attached to a post. One can either store it as a binary blob in the database or store it in the filesystem and then make a reference. Blobs in databases are considered harmful because they make dumps bigger; and considering Redis' in-memory storage scheme, the memory usage may grow too big.

Attachments will be probably better served in a static server-accessible folder to serve them faster than CGI could output the data (HTTP servers often use tricks to speed up static file transfer).

* Workflows
** Posting
1. User POSTs to Micropub endpoint
2. Micropub endpoint converts the input to the canonical h-entry JSON form
3. Set post["published"] to the current date if it is not set
4. Save attachments to upload folder if they were attached verbatim (I think specification allows that stuff)
5. ~redis.hset("posts", post["properties"]["mp-slug"] or tounix(post["properties"]["published"]), json.dumps(post))~
6. ~redis.lpush("posts_order", get_slug(post))~
7. ~redis.sadd("categories", *post["category"])~
7. ~redis.publish("posts", post["slug"] or tounix(post["published"]))~

After that, a static renderer frontend subscribed to posts channel would receive a message with a post ID. Then:
1. It would fetch the post from the DB
2. It would generate/update the post page
3. It would update the tag taxonomies for the post
4. It would update the main feed with the new post, regenerating all pages
5. It would push the changes to master (Netlify maybe? That would be extremely cool!)

A dynamic renderer wouldn't need to act upon the post, except for flushing caches for pages. By the way, maybe caching HTML in Redis can help?

** Rendering posts
1. User sends a request to view a page
2. Renderer fetches the post from the database
3. Renderer fetches the template for the post
4. Renderer applies the template to a post JSON entry
5. Renderer replies with resulting MF2 HTML to the user

** IndieAuth
1. User sends an IndieAuth request
2. The IndieAuth auth page checks for identity (using the password from database and/or some other methods of verification)
3. The IndieAuth does the authy thing and redirects the user, confirming their identity

** Webmentions
1. The author posts a post (see [[Posting][Posting]] for more info)
2. A user sees a post
3. A user comments on their blog and sends a webmention
4. ~target_post = micropub.read_post(get_slug_from_url(target))~
5. Endpoint checks for existing comment with that URL and if it finds it, it updates that comment. If not, it adds it.
  1) ~target_post["properties"]["comment"].append(mf2py.parse(source))~
  2) ~target_post["properties"]["comment"][target_post["properties"]["comment"].index(list(filter(lambda i: source in i["properties"]["url"], target_post["properties"]["comment"]))[0])] = mf2py.parse(source)["items"][0]~ - This thing actually:
    1. Gets the comment number from source URL
    2. Fetches the updated comment's post
    3. Rewrites the comment to contain new comments/updated content
7. ~micropub.write_post(target_post)~

In case of receiving a salmention, the whole process just repeats itself - since the salmentions are rendered as comments to comments, they're fairly easy to implement.
