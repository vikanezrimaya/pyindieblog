{ system ? builtins.currentSystem }:
let
  pkgs = (import <nixpkgs>) {
    overlays = [
      (import nix/overlay.nix)
    ];
  };
  #forAllSystems = genAttrs supportedSystems;
  #importTest = fn: args: system: import fn ({
  #  inherit system;
  #} // args);
  #callTest = fn: args: forAllSystems (system: hydraJob (importTest fn args system));
  makeTest = import <nixpkgs/nixos/tests/make-test.nix>;
in rec {
  pyindieblog = pkgs.pyindieblog;
  mf2py = pkgs.python3Packages.mf2py;
  mf2util = pkgs.python3Packages.mf2util;
  pyindieblog-channel = pkgs.releaseTools.channel {
    constituents = [ pyindieblog ];
    name = "pyindieblog";
    src = ./.;
  };
  flask-indieauth = pkgs.python3Packages.flask-indieauth;
  flask-hashfs = pkgs.python3Packages.flask-hashfs;
  hashfs = pkgs.python3Packages.hashfs;
  spano = pkgs.python3Packages.spano;
  pyindieblog-smoke-test = makeTest ({ pkgs, ... }: {
    name = "pyindieblog-smoke-test";
    machine = { lib, pkgs, ... }: {
      environment.systemPackages = with pkgs; [
        curl jq gnugrep
      ];
      imports = [ ./configuration.nix ];
    };
    testScript = { nodes, ... }: ''
      $machine->waitForUnit("uwsgi.service");
      $machine->waitForUnit("nginx.service");
      $machine->waitForUnit("default.target");
      # Smoke test! Checks that all mount points are in place and responding correctly.
      # Renderer should at least return 200 OK
      $machine->succeed("curl --silent -i http://localhost/ | grep -i '200 OK'");
      # Media Endpoint should return 405 Method Not Allowed
      $machine->succeed("curl --silent -i http://localhost/upload_media/ | grep -i '405 Method Not Allowed'");
      # Micropub responds with valid JSON on error
      # errors include incorrect requests, e.g. without parameters or a token
      $machine->succeed("curl --silent http://localhost/micropub/ | jq .");
    ''; 
  });
}
