{ config, pkgs, lib, ... }:
{
  systemd.services = {
    uwsgi = {
      after = ["pyindieblog-renderer-token-key.service" "pyindieblog-init.service"];
      wants = ["pyindieblog-renderer-token-key.service" "pyindieblog-init.service"];
    };
    pyindieblog-init = {
      path = with pkgs; [ coreutils ];
      script = ''
        [[ -e /var/lib/pyindieblog && -e /var/lib/spano ]] && exit
        mkdir -p /var/lib/pyindieblog /var/lib/spano/uploads
        chown -R uwsgi: /var/lib/pyindieblog /var/lib/spano
        chmod -R 750 /var/lib/pyindieblog /var/lib/spano
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
    };
  };
  nixpkgs.overlays = [(import nix/overlay.nix)];
  services.redis = {
    enable = true;
    bind = "127.0.0.1";
  };
  services.nginx = {
    enable = true;
    virtualHosts = {
      "fireburn.ru" = {
        extraConfig = ''
          http2_push_preload on;
        '';
        locations = {
          "/" = {
            extraConfig = ''
              include ${config.services.nginx.package}/conf/uwsgi_params;
              if ($request_method = 'OPTIONS') {
                  add_header 'Access-Control-Allow-Origin' '*';
                  add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                  add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                  add_header 'Access-Control-Max-Age' 1728000;
                  add_header 'Content-Type' 'text/plain; charset=utf-8';
                  add_header 'Content-Length' 0;
                  return 204;
              }
              if ($request_method = 'POST') {
                  add_header 'Access-Control-Allow-Origin' '*';
                  add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                  add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                  add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
              }
              if ($request_method = 'GET') {
                  add_header 'Access-Control-Allow-Origin' '*';
                  add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                  add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                  add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
              }
              uwsgi_pass unix://${config.services.uwsgi.runDir}/pyindieblog-renderer.sock;
            '';
          };
          "/micropub" = {
            extraConfig = ''
              include ${config.services.nginx.package}/conf/uwsgi_params;
              if ($request_method = 'OPTIONS') {
                  add_header 'Access-Control-Allow-Origin' '*';
                  add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                  add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
                  add_header 'Access-Control-Max-Age' 1728000;
                  add_header 'Content-Type' 'text/plain; charset=utf-8';
                  add_header 'Content-Length' 0;
                  return 204;
              }
              if ($request_method = 'POST') {
                  add_header 'Access-Control-Allow-Origin' '*';
                  add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                  add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
                  add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range,Location';
              }
              if ($request_method = 'GET') {
                  add_header 'Access-Control-Allow-Origin' '*';
                  add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                  add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
                  add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range,Location';
              }
              uwsgi_pass unix://${config.services.uwsgi.runDir}/pyindieblog-micropub.sock;
            '';
          };
          "/upload_media" = {
            extraConfig = ''
              include ${config.services.nginx.package}/conf/uwsgi_params;
              if ($request_method = 'OPTIONS') {
                  add_header 'Access-Control-Allow-Origin' '*';
                  add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                  add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
                  add_header 'Access-Control-Max-Age' 1728000;
                  add_header 'Content-Type' 'text/plain; charset=utf-8';
                  add_header 'Content-Length' 0;
                  return 204;
              }
              if ($request_method = 'POST') {
                  add_header 'Access-Control-Allow-Origin' '*';
                  add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                  add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
                  add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range,Location';
              }
              uwsgi_pass unix://${config.services.uwsgi.runDir}/spano.sock;
            '';
          };
          "/media/" = {
            alias = "/var/lib/spano/uploads/";
          };
        };
      };
    }; 
  };
  # Needed for nginx to access uwsgi sockets
  users.groups.uwsgi.members = ["nginx"];
  # Needed for tokens!
  users.groups.keys.members = ["uwsgi"];
  services.uwsgi = {
    enable = true;
    plugins = [ "python3" ];
    instance = {
      type = "emperor";
      vassals = let
        mkVassal = name: args: args // {
          type = "normal";
          chmod-socket = "770";
          socket = "${config.services.uwsgi.runDir}/${name}.sock";
        };
        spano_config = pkgs.writeTextFile {
          name = "spano.cfg";
          text = ''
            ME = "https://fireburn.ru"
            TOKEN_ENDPOINT = "https://tokens.indieauth.com/token"
            HASHFS_HOST = None
            HASHFS_PATH_PREFIX = '/media'
            HASHFS_ROOT_FOLDER = '/var/lib/spano/uploads'
            HASHFS_DEPTH = 4
            HASHFS_WIDTH = 2
            HASHFS_ALGORITHM = 'sha256'
          '';
        };
      in {
        spano = mkVassal "spano" {
          plugin = "python3";
          pythonPackages = p: with p; [ spano ];
          manage-script-name = true;
          env = ["SPANO_CONFIG=${spano_config}"];
          mount = [
            "/upload_media=spano:create_app()"
          ];
          master = true;
          processes = 4;
          threads = 4;
        };
        pyindieblog-micropub = mkVassal "pyindieblog-micropub" {
          plugin = "python3";
          env = [
            "PYINDIEBLOG_MICROPUB_CONFIG=${pkgs.writeTextFile {
              name = "pyindieblog-micropub.py";
              text = ''
                from pyindieblog.micropub.backends import RedisDatabase
                # This file is a config file, but it's also Python code.
                # You can access all of pyindieblog here.
                DATABASE=RedisDatabase("${config.services.redis.bind}")
                TOKEN_ENDPOINT="https://tokens.indieauth.com/token"
                MEDIA_ENDPOINT="https://fireburn.ru/upload_media/"
                IDENTITY="fireburn.ru"
                SYNDICATE_TO=[
                  {
                    "uid": "https://brid.gy/publish/twitter",
                    "name": "Twitter",
                    "service": {
                      "name": "Twitter",
                      "url": "https://twitter.com",
                      "photo": "https://fireburn.ru/media/1b/e5/d0/5c/e6faad469f7f9c5a5879f2d9f8d267b60eb394e92c19217268bcea8f.png"
                    },
                    "user": {
                      "name": "kisik21",
                      "url": "https://twitter.com/kisik21",
                      "photo": "https://fireburn.ru/media/f1/5a/fb/9b/081efafb97b4ad59f5025cf2fd0678b8f3e20e4c292489107d52be09.png"
                    }
                  },
                  {
                    "uid": "https://news.indieweb.org/en",
                    "name": "IndieNews (en)"
                  }
                ]
              '';
            }}"
          ];
          pythonPackages = p: with p; [ pyindieblog spano ];
          manage-script-name = true;
          mount = [
            "/micropub=pyindieblog.micropub:get_app()"
          ];
          master = true;
          processes = 4;
          threads = 4;
        };
        pyindieblog-renderer = mkVassal "pyindieblog-renderer" {
          plugin = "python3";
          env = [
            "PYINDIEBLOG_RENDERER_CONFIG=${pkgs.writeTextFile {
              name = "pyindieblog-renderer.py";
              text = ''
                # This file is a config file, but it's also Python code.
                # You can access all of pyindieblog here.
                import sys
                TOKEN_ENDPOINT="https://tokens.indieauth.com/token"
                MICROPUB_ENDPOINT="https://fireburn.ru/micropub/"
                AUTHORIZATION_ENDPOINT="https://indieauth.com/auth"
                MICROSUB_ENDPOINT="https://aperture.p3k.io/microsub/158"
                WEBMENTION_ENDPOINT="https://webmention.io/fireburn.ru/webmention"
                MEDIA_ENDPOINT="https://fireburn.ru/upload_media/"
                # Redirect for older versions of the software.
                LEGACY_REDIRECT=True
                try:
                    with open("/run/keys/kittybox-access-token") as tokenfile:
                        MICROPUB_ACCESS_TOKEN=tokenfile.read().strip()
                except Exception as e:
                    print("Exception occured when retrieving token:", e, file=sys.stderr)
                    MICROPUB_ACCESS_TOKEN="" # An empty string signifies bootstrapping stage.
                    # The h-card is available, but posts are not.
                    # No micropub requests are made.
                ME={
                    "name": "Vika's Hideout",
                    "subtitle": "A blog of a certain programmer girl",
                    "footer": '&copy; Vika. Some rights reserved.<br><a href="https://xn--sr8hvo.ws/%E2%98%BA%EF%B8%8F%F0%9F%94%92%F0%9F%8D%BD/previous">←</a> 🕸💍 <a href="https://xn--sr8hvo.ws/%E2%98%BA%EF%B8%8F%F0%9F%94%92%F0%9F%8D%BD/next">→</a>',
                    "author": {
                        "type": ["h-card"],
                        "properties": {
                            "name": ["Vika"],
                            "url": ["https://fireburn.ru/"],
                            "photo": [
                              "https://fireburn.ru/media/f1/5a/fb/9b/081efafb97b4ad59f5025cf2fd0678b8f3e20e4c292489107d52be09.png"
                            ],
                            "pronoun": ["she/her", "they/them"],
                            "email": ["vika@fireburn.ru"],
                            "note": ["A trans-person and a clueless amateur coder. Doesn't like her look and uses an anime-styled avatar instead."]
                        },
                        "_rel_me": [
                          "https://twitter.com/kisik21",
                          "https://github.com/kisik21",
                          "https://keybase.io/kisik21"
                        ]
                    }
                }
              '';
            }}"
          ];
          pythonPackages = p: with p; [ pyindieblog ];
          mount = [
            "/=pyindieblog.renderer:get_app()"
          ];
          master = true;
          processes = 4;
          threads = 4;
        };
      };
    };
  };
}
