{
  description = "pyindieblog - the blog software that powers fireburn.ru";

  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs-channels";
      ref = "nixos-unstable";
    };
  };

  outputs = { self, nixpkgs }: let
    systems = [ "x86_64-linux" "aarch64-linux" ];
    forAllSystems = function: nixpkgs.lib.genAttrs systems function;
  in {
    overlay = import ./nix/overlay.nix;
    nixosModules.pyindieblog = import ./configuration.nix;
    apps = forAllSystems (system: let
      pkg = (import nixpkgs { overlays = [self.overlay]; inherit system; }).pyindieblog;
    in {
      pyindieblog-micropub = {
        type = "app";
        program = "${pkg}/bin/pyindieblog-micropub";
      };
      pyindieblog-renderer = {
        type = "app";
        program = "${pkg}/bin/pyindieblog-renderer";
      };
    });
  };
}
