#!/usr/bin/env python3
import json
import logging
import os
import sys
import urllib
import dateutil.parser
import mf2py
import requests
from datetime import datetime, timezone, timedelta
from html.parser import HTMLParser
from markdown import Markdown
from flask import jsonify, request, config, g, Flask, Blueprint, current_app
from pyindieblog import send_webmention, validate_token, is_test_db
from pyindieblog.micropub.backends import DBConflictException
from pyindieblog.micropub.helpers import *
USER_KNOWS_WHAT_ARE_THEY_DOING_TOKEN = "Yes, I know what am I doing!"

markdown = Markdown(extensions=['attr_list', 'fenced_code', 'smarty', 'sane_lists'])

def run():
    get_app().run(port=3000)

def get_app(test_db=None):
    try:
        instance_path = os.getenv("PYINDIEBLOG_MICROPUB_INSTANCE")
    except IndexError:
        instance_path = None
    app = Flask(__name__, instance_path=instance_path)
    if app.debug or test_db:
        logging.basicConfig(level=logging.DEBUG)
        app.logger.debug(f"Running in debug mode, app.debug={app.debug}, test_db={test_db}")
        if test_db is None:
            from pyindieblog.micropub.backends import RedisDatabase
            test_db = RedisDatabase("localhost")
        app.config.from_mapping(
            DATABASE=test_db,
            SECRET_KEY="dev",
            SERVER_NAME="localhost:3000",
            IDENTITY="fireburn.ru",
            TOKEN_ENDPOINT=None,
            TESTING=True
        )
    else:
        configfile = "micropub_config.py"
        try:
            app.config.from_envvar("PYINDIEBLOG_MICROPUB_CONFIG")
        except FileNotFoundError:
            app.logger.warning("Can't load config from envvar, searching in instance folder...")
            app.config.from_pyfile(os.path.join(app.instance_path, configfile))
        if "NO_TOKEN_ENDPOINT" in app.config:
            app.logger.critical("YOU ARE RUNNING WITHOUT A TOKEN ENDPOINT CONFIGURED.")
            if app.config["NO_TOKEN_ENDPOINT"] != USER_KNOWS_WHAT_ARE_THEY_DOING_TOKEN:
                app.logger.critical("This may have been a huge mistake, so the software will NOT run.")
                app.logger.critical("A token endpoint protects you from random people wrecking havoc on your database and it is REQUIRED by Micropub specification.")
                app.logger.critical("Without the token endpoint, the software will just accept ANY request whatsoever without checking if it's legitimate.")
                app.logger.critical("You may not know what are you doing. To prove that you do, and you take responsibility for ALL of your actions, set the NO_TOKEN_ENDPOINT parameter to this exact string:\n\n\n%s\n\n", USER_KNOWS_WHAT_ARE_THEY_DOING_TOKEN)
                app.logger.critical("I AM NOT RESPONSIBLE FOR POSSIBLE ATTACKS, DATA LOSS, HEAT DEATH OF THE UNIVERSE OR ANY OTHER EVENTS WRECKING YOUR DATABASE. YOU HAVE BEEN WARNED.")
                exit(1)
            app.logger.critical("NO AUTHENTICATION NOR AUTHORIZATION OF REQUESTS WILL BE PERFORMED. I AM NOT RESPONSIBLE FOR POSSIBLE ATTACKS, DATA LOSS, HEAT DEATH OF THE UNIVERSE OR ANY OTHER EVENTS WRECKING YOUR DATABASE. YOU HAVE BEEN WARNED.")
            app.logger.critical("REMOVE ANY MENTIONS OF NO_TOKEN_ENDPOINT IN YOUR CONFIG TO ENABLE TOKEN ENDPOINT AUTHENTICATION BACK.")
    # Register blueprint for micropub
    app.register_blueprint(micropub_blueprint)
    return app

def send_webmentions(post):
    for target in g.webmentions:
        try:
            send_webmention(post["properties"]["url"][0], target)
        except requests.exceptions.ConnectionError:
            # When testing, we may be in Nix sandbox.
            # It does not have an internet connection.
            current_app.logger.warning("Couldn't send webmention for %s to %s",
                                       post["properties"]["url"][0], target)

def ping_hub(post):
    pass

def syndicate_to(post, target):
    if urllib.parse.urlparse(target).hostname == "twitter.com":
        # TODO twitter syndication
        # We can use brid.gy publish for this
        # But I dunno, maybe it could be built-in
        pass
    else:
        result_url = send_webmention(post["properties"]["url"], target)
        post["properties"].setdefault("syndication", []).append(result_url)

def syndicate(post):
    syndicate_to_list = post["properties"].get("mp-syndicate-to", [])
    while len(syndicate_to_list) > 0:
        target = syndicate_to_list.pop()
        syndicate_to(post, target)
    write_post(post, update=True)


def convert_markdown(post):
    if isinstance(post["properties"].get("content", [None])[0], str):
        # Convert strings using Markdown into HTML.
        post["properties"]["content"] = [{
            "html": markdown.reset().convert(post["properties"]["content"][0]),
            "value": post["properties"]["content"][0]
        }]

class WebmentionLinkSender(HTMLParser):
    def handle_starttag(self, tag, attrs):
        link = ""
        if tag == "a":
            for k, v in attrs:
                if k == "href":
                    link = v
                if k == "rel" and v == "nofollow":
                    return
            g.webmentions.update([link])

def gather_webmentions(post):
    if "webmentions" not in g:
        g.webmentions = set()
    g.webmentions.update(
        # Only accept strings - because we might have already sent webmentions to these posts if they're POSTS.
        list(filter(lambda i: isinstance(i, str), post["properties"].get("in-reply-to", []))) +
        list(filter(lambda i: isinstance(i, str), post["properties"].get("like-of", []))) +
        list(filter(lambda i: isinstance(i, str), post["properties"].get("repost-of", []))) +
        list(filter(lambda i: isinstance(i, str), post["properties"].get("bookmark-of", [])))
        # TODO filter out URLs in the post itself for usual mentions
    )
    if "content" in post["properties"]:
        WebmentionLinkSender().feed(post["properties"]["content"][0]["html"] if isinstance(post["properties"]["content"][0], dict) else post["properties"]["content"][0])


def auto_syndicate_silo_replies(post):
    for link in g.webmentions:
        if urllib.parse.urlparse(link).hostname == "twitter.com":
            if "https://brid.gy/publish/twitter" not in post["properties"].setdefault("mp-syndicate-to", []):
                post["properties"]["mp-syndicate-to"].append("https://brid.gy/publish/twitter")
        
        
def normalize_post(data):
    # Make stuff mutable
    data = data.copy()
    if "access_token" in data:
        # We should not keep access token in the posts
        del data["access_token"]
    if "h" in data:
        # we have a form-encoded dictionary. Normalize it to JSON.
        post = {"type": ["h-{}".format(data.get("h"))], "properties": {}}
        for entry in data:
            if entry == "h":
                continue
            elif "[]" in entry:
                name = entry.strip("[]")
            else:
                name = entry
            post["properties"].update({name: getformlist(data, entry)})
    else:
        post = data
    current_app.logger.debug(post)
    if "published" not in post["properties"]:
        post["properties"]["published"] = [iso_time()]
    if "visibility" not in post["properties"]:
        post["properties"]["visibility"] = ["public"]
    if post["properties"]["visibility"] == "private":
        # Add the person you're replying to to the list of audience so that person can read the post.
        post["properties"].setdefault("audience", []).extend(
            map(
                lambda u: u if isinstance(u, str) else u["properties"]["url"][0],
                post["properties"].get("in-reply-to")
            )
        )
    if "url" not in post["properties"]:
        # This defines a clear URL scheme.
        post["properties"]["url"] = [
            "https://{domain}/{dir}/{slug}".format(
                domain=current_app.config["IDENTITY"],
                dir=("cards" if post["type"] == "h-card" else "posts"),
                slug=getslug(post)
            )
        ]
        if "mp-slug" in post["properties"]:
            # TODO check for slug collisions
            "https://{domain}/{dir}/{slug}".format(
                domain=current_app.config["IDENTITY"],
                dir=("cards" if post["type"] == "h-card" else "posts"),
                slug=post["properties"]["mp-slug"][0]
            )
    return post

def include_media(post):
    for photo in g.uploaded_files:
        post["properties"].setdefault("photo", []).append(photo)

process_hooks = [
    normalize_post, # DO NOT DELETE THIS HOOK IT IS CRITICAL FOR PROCESSING FORM-ENCODED POSTS!!!
    convert_markdown, # This should be before gathering webmentions because webmention hook processes HTML
    gather_webmentions,
    auto_syndicate_silo_replies,
    include_media,
]

post_write_hooks = [
    # These hooks are called after an initial version of the post is written
    # into the database. They are called in a specified order and can use
    # write_post(post, update=True) function to rewrite the post in the database, if needed.
    syndicate,
    ping_hub,
    send_webmentions,
]

def write_post(*args, **kwargs):
    return current_app.config["DATABASE"].write_post(*args, **kwargs)

def read_post(*args, **kwargs):
    # Load and parse the JSON representation of a certain post in the database.
    return current_app.config["DATABASE"].read_post(*args, **kwargs)

def getformlist(form, name):
    """Get a value in MF2-JSON-compatible form (a list with 0 or more values),
    extracting it from a Werkzeug Form object via Form.getlist() method.

    Note that some clients send arrays without [] and some clients send some
    values always as []-suffixed arrays. Either way, we'll get both to avoid
    confusion and maximize compatibility."""
    return form.getlist(name) + form.getlist("{}[]".format(name))

def accept_POST():
    # We're assuming that Flask is used here since I'm fairly used to Flask (and Django seems an overkill for this).
    json_data = request.get_json()
    if not (is_test_db() or current_app.debug or current_app.testing or "NO_TOKEN_ENDPOINT" in current_app.config):
        # Get the token and validate it
        token = request.form.get("access_token") or request.headers.get("Authorization", "Bearer ").split(' ')[1]
        valid, g.token_data = validate_token(token, current_app.config["TOKEN_ENDPOINT"])
        if not (valid and urllib.parse.urlparse(g.token_data["me"]).netloc == current_app.config["IDENTITY"]):
            # TODO error return
            return jsonify({"error": "invalid token"}), 401
    if isinstance(json_data, dict):
        if "access_token" in json_data:
            del json_data["access_token"]
    current_app.logger.debug(json_data)
    current_app.logger.debug(request.form)
    post = json_data or request.form
    current_app.logger.debug(post)
    if not "action" in post: # We're creating something.
        # Deny everything without an access token authorized for creating new posts.
        if not (is_test_db() or current_app.debug or current_app.testing or "NO_TOKEN_ENDPOINT" in current_app.config):
            if "create" not in g.token_data["scope"]:
                return jsonify({"error": "insufficient_scope"}), 401
        # If Indigenous tries to upload photos...
        # @swentel plz use the media endpoint TT__TT
        # It has specialized storage for that kind of things
        g.uploaded_files = []
        if len(request.files) > 0:
            for media in request.files.getlist("photo") + request.files.getlist("photo[]"):
                upload = requests.post(current_app.config["MEDIA_ENDPOINT"], files={"file": media}, headers={"Authorization": "Bearer " + token})
                g.uploaded_files.append(upload.headers["Location"])
        current_app.logger.debug("Data before preprocessing: %s", json.dumps(post))
        for hook in process_hooks:
            post = hook(post) or post
        current_app.logger.debug("Data being written (1st pass): %s", json.dumps(post))
        url = write_post(post)
        for hook in post_write_hooks:
            hook(post)
        current_app.logger.debug("Data after the post-processing: %s", json.dumps(post))
        return "", 201, {"Location": url or post["properties"]["url"][0]}
    else:
        if post["action"] == "update":
            if not (is_test_db() or current_app.debug or current_app.testing or "NO_TOKEN_ENDPOINT" in current_app.config):
                if "update" not in g.token_data["scope"]:
                    return jsonify({"error": "insufficient_scope"}), 401
            #post = read_post(post["url"])
            #for i in post.get("add"):
        elif post["action"] == "delete":
            if not (is_test_db() or current_app.debug or current_app.testing or "NO_TOKEN_ENDPOINT" in current_app.config):
                if "delete" not in g.token_data["scope"]:
                    return jsonify({"error": "insufficient_scope"}), 401
            try:
                post = read_post(post["url"])
            except KeyError:
                return {"error": "invalid_request"}, 400
            except FileNotFoundError:
                return {"error": "not_found"}, 404
            post["properties"].setdefault("deleted", []).append(iso_time())
            write_post(post, update=True)
            return jsonify({"success": True}), 200
        elif post["action"] == "undelete":
            if not (is_test_db() or current_app.debug or current_app.testing or "NO_TOKEN_ENDPOINT" in current_app.config):
                if "delete" not in g.token_data["scope"]:
                    return jsonify({"error": "insufficient_scope"}), 401
            try:
                post = read_post(post["url"])
            except KeyError:
                return {"error": "invalid_request"}, 400
            except FileNotFoundError:
                return {"error": "not_found"}, 404
            del post["properties"]["deleted"]
            write_post(post, update=True)
            return jsonify({"success": True}), 200

def accept_GET():
    db = current_app.config.get("DATABASE")
    if not (is_test_db() or current_app.debug or current_app.testing or "NO_TOKEN_ENDPOINT" in current_app.config):
        token = request.headers.get("Authorization", "Bearer ").split(' ')[1]
        valid, token_data = validate_token(token, current_app.config["TOKEN_ENDPOINT"])
        if not (valid and urllib.parse.urlparse(token_data["me"]).netloc == current_app.config["IDENTITY"]):
            return jsonify({"error": "invalid token", "valid": valid, "token_data": token_data}), 401
    if request.args.get("q") == "config":
        config = {
            "q": [
                "config",
                "syndicate-to",
                "geo",
                "source"
            ]
        }
        if current_app.config.get("MEDIA_ENDPOINT"):
            config.update({'media-endpoint': current_app.config.get("MEDIA_ENDPOINT")})
        if current_app.config.get("SYNDICATE_TO"):
            config.update({"syndicate-to": current_app.config.get("SYNDICATE_TO")})
        return jsonify(config)
    elif request.args.get("q") == "syndicate-to":
        return jsonify({"syndicate-to": current_app.config.get("SYNDICATE_TO")})
    elif request.args.get("q") == "geo":
        return db.georadius(request.args["lat"], request.args["lon"])
    elif request.args.get("q") == "source":
        if "url" in request.args:
            try:
                post = db.read_post(request.args["url"])
            except FileNotFoundError:
                return jsonify({"error": "not found"}), 404
            if len(getformlist(request.args, "properties")):
                props = {}
                for i in getformlist(request.args, "properties"):
                    props[i] = post["properties"][i]
                    return jsonify(props)
            return jsonify(post)
        else:
            return jsonify(db.post_list(int(request.args.get("limit", 10)), before=request.args.get("before"), after=request.args.get("after")))
    else:
        return jsonify({"error": "invalid_request"}), 400


micropub_blueprint = Blueprint('micropub', __name__)
@micropub_blueprint.route('/', methods=['GET', 'POST'])
def process_request():
    if request.method == "GET":
        return accept_GET()
    elif request.method == "POST":
        if current_app.config.get("READONLY", False):
            return jsonify({"error": "readonly"}), 503
        return accept_POST()
    else:
        return jsonify({"error": "invalid_request"}), 400


# These are actually pyindieblog's internal API endpoints.
# These are not standardized. I just think it's kinda cool to have them. Maybe.
# Redeploying configuration every single time I change my h-card doesn't sound very cool.
@micropub_blueprint.route('/me', methods=['GET', 'POST'])
def author_hcard():
    if not (is_test_db() or current_app.debug or current_app.testing or "NO_TOKEN_ENDPOINT" in current_app.config):
        token = request.headers.get("Authorization", "Bearer ").split(' ')[1]
        valid, token_data = validate_token(token, current_app.config["TOKEN_ENDPOINT"])
        if not (valid and urllib.parse.urlparse(token_data["me"]).netloc == identity):
            return jsonify({"error": "invalid token", "valid": valid, "token_data": token_data}), 401
    db = current_app.config['DATABASE']
    if request.method == "GET":
        return db.get("me")
    else:
        db.set("me", json.dumps(request.get_json()))

if __name__ == '__main__':
    run()
