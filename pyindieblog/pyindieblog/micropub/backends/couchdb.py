"""CouchDB backend for pyindieblog."""
import os
import json
import urllib
import requests
from pyindieblog.micropub.helpers import getslug
from pyindieblog.micropub.backends.null import Database, DBConflictException


class CouchDatabase(Database):
    """CouchDB backend."""

    def __init__(self, uri=None, auth=None):
        self.uri = uri or "http://localhost:5984"
        self.auth = auth

    def read_post(self, url):
        key, postid = url.split("/")[-2:]
        request = requests.get(os.path.join(self.uri, key, postid))
        if request.status_code == 404:
            raise FileNotFoundError

        return request.json()

    # pylint: disable=inconsistent-return-statements
    def write_post(self, post):
        if post["type"][0] == "h-card":
            if "mp-slug" in post["properties"]:
                post_id = post["properties"]["mp-slug"][0]
            else:
                post_id = post["properties"]["name"][0].replace(
                    " ", "_").replace("/", "_")
            request = requests.post(
                urllib.parse.urljoin(self.uri, "posts", post_id),
                post
            )
            if request.status_code == 409:
                # Conflict!
                raise DBConflictException
            return f"/hcards/{post_id}"
        # Generate a post slug
        post_id = getslug(post)
        # Write post JSON to the database
        request = requests.post(
            urllib.parse.urljoin(self.uri, f"/posts/{post_id}"),
            post
        )
        if request.status_code == 409:
            # Conflict!
            raise DBConflictException

    @staticmethod
    def _generate_paging(limit=10, before=None, after=None):
        paging = {}
        if after:
            paging["after"] = after + limit
        if before:
            paging["before"] = before - limit
        return paging

    def postlist(self, lim=10, after=None, before=None, auth=None):
        """Fetch a list of posts."""
        query = {"selector": {}, "limit": lim,
                 "sort": [{"properties.published": "desc"}],
                 "use_index": "mainfeed"}
        # This is a simple selector for finding public posts.
        query["selector"] = {"properties": {
            "visibility": {
                "$elemMatch": {
                    "$eq": "public"
                }
            }
        }}
        if auth:
            # This is a more complex selector.
            # It works when authenticated, and finds either:
            #  - public posts
            #  - private posts without audience (i.e. requiring just IndieAuth)
            #  - private posts intended for a certain person
            query["selector"] = {"$or": [
                {"properties": {
                    "visibility": {
                        "$elemMatch": {
                            "$eq": "public"
                        }
                    }
                }},
                {
                    "properties.visibility": {
                        "$elemMatch": {
                            "$in": ["private"]
                        }
                    },
                    "$or": [
                        {"properties.audience": {"$exists": False}},
                        {"properties.audience": {"$elemMatch": {"$eq": auth}}}
                    ]
                }
            ]}
        query["selector"].setdefault("properties", {})["like-of"] = {
            "$exists": False
        }
        if after:
            query["selector"]["properties"]["published"] = {"$gte": after}
        elif before:
            query["selector"]["properties"]["published"] = {"$lte": before}
        request = requests.post(self.uri + "/posts/_find", json=query)
        if request.status_code == 200:
            j = request.json()
            return {
                "items": j["docs"],
                "paging": self._generate_paging(lim, before, after)
            }
        if request.status_code == 400:
            err = request.json()
            if err["error"] == "no_usable_index":
                raise Exception(err['reason'] +
                                f"Query was: {json.dumps(query)}")
        raise Exception(request)
