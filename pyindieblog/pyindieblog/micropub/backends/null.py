class DBConflictException(Exception):
    pass
    
class Database:
    """Empty database backend."""
    def read_post(self, url):
        pass
    def write_post(self, post, update=False):
        pass
    def post_list(self, limit=10, before=None, after=None, auth=None):
        pass
    def georadius(self, lat, lon):
        pass

