"""Backend for flat files indexed by Redis."""
from os import path
import json
import urllib
import redis
from flask import current_app, jsonify
from pyindieblog.micropub.backends.null import Database
from pyindieblog.micropub.helpers import getslug


class RedisCachedFlatFiles(Database):
    """Flat files backend assisted by Redis.

    Redis stores copies of post metadata, indexes allowing for tag searches and
    quick feed building without actually reading ALL the files, which is slow.

    Flat files provide resillience, since indexes can be easily rebuilt from
    them. Redis provides speed, since the index allows for quick retrieval of
    metadata, e.g. ordering posts or building tagged feeds.

    You could probably point several endpoints at the same dir (e.g. network
    mount), but this is not recommended, as there is no locking implemented."""

    def __init__(self, redis_uri="", db=None, datadir="/var/lib/pyindieblog"):
        if db is not None:
            self.redis = db
        else:
            self.redis = redis.Redis(redis_uri)

        self.datadir = datadir

    def _read_file(self, postid, key="posts"):
        with open(path.join(self.datadir, key, postid + ".json")) as jsonfile:
            return json.loads(jsonfile.read())

    def read_post(self, url):
        key, postid = url.split("/")[-2:]
        slug = self.redis.hget("slugs", postid)
        if slug:
            postid = slug
        return self._read_file(postid, key)

    # pylint: disable=inconsistent-return-statements,too-many-branches
    def write_post(self, post, update=False):
        if post["type"][0] == "h-card":
            key = "hcards"
            if "mp-slug" in post["properties"]:
                postid = post["properties"]["mp-slug"][0]
            else:
                postid = post["properties"]["name"][0]\
                    .replace(" ", "_").replace("/", "_").lower()
            with open(path.join(self.datadir, key, f"{postid}.json"),
                      'w') as jsonfile:
                jsonfile.write(json.dumps(post))
            for geo in post["properties"].get("geo", []):
                # Add it to the geoindex
                self.redis.geoadd(
                    "venues",
                    float(geo["properties"]["longitude"][0]),
                    float(geo["properties"]["latitude"][0]),
                    f"https://{current_app.config['IDENTITY']}/hcards/{postid}"
                )
            return f"/hcards/{postid}"
        # Generate a post slug
        post_id = getslug(post)
        # Assume we're posting a post to a main feed here.
        # We can handle other cases in another branch.
        # This lowers code complexity, making Pylint happy :3
        key = "posts"
        if "url" in post["properties"]:
            key = urllib.parse.urlparse(post["properties"]["url"][0])\
                                .path.strip('/').split('/')[0]
        # Write post JSON to the database
        with open(path.join(self.datadir, key, post_id + ".json"),
                  'w') as jsonfile:
            jsonfile.write(json.dumps(post))
        if "deleted" in post["properties"]:
            self.redis.lrem("mainfeed", 0, post_id)
        if not update:
            if key == "posts":
                # Push posts into feeds.
                # To speed up feed creation, feeds are cached in Redis..
                if "public" in post["properties"]["visibility"]:
                    # Public posts are pushed into the main feed
                    self.redis.lpush("mainfeed", post_id)
                    # and also in the "authenticated user" feed
                    self.redis.lpush("privateposts", post_id)
                    # Here we should probably post things for users
                    for user in self.redis.smembers("otherusers"):
                        self.redis.lpush("postsfor_{user}", post_id)
                elif "private" in post["properties"]["visibility"]:
                    if "audience" in post["properties"]:
                        for user in post["properties"]["audience"]:
                            self.redis.lpush(f"postsfor_{user}", post_id)
                            self.redis.sadd("otherusers", user)
                    else:
                        # privateposts feed is for posts that just require auth
                        # but are not neccesarily very private.
                        # If you want true privacy, set audience property.
                        self.redis.lpush("privateposts", post_id)
                # Add the hashtags to the autocomplete list
                list(map(
                    lambda i: self.redis.sadd("categories", i),
                    post["properties"].get("category", [])
                ))
                # Add slugs to the slugs array
                for slug in post["properties"].get("mp-slug", []):
                    self.redis.hset("slugs", slug, post_id)

    def post_list(self, limit=10, before=None, after=None, auth=None):
        # Determine authentication
        feed = "mainfeed"
        if self.redis.sismember('otherusers', auth):
            feed = "postsfor_" + auth
        # Load the list of post keys from Redis
        postlist = list(map(
            lambda i: i.decode('utf-8'), self.redis.lrange(feed, 0, -1)
        ))
        if after is not None:
            beginning = postlist.index(after)
            end = beginning + limit
        elif before is not None:
            end = postlist.index(before)
            beginning = end - limit
        else:
            beginning = 0
            end = limit
        if beginning < 0:
            beginning = 0
        # Apply the limits to get the current page
        filtered = postlist[beginning:end]
        if len(filtered) == 0:
            # If we have no posts, return just an empty list and no paging.
            return {"items": []}
        # If we do have posts, get them from the database.
        items = list(map(self._read_file, filtered))
        paging = {}
        if len(items) != len(postlist):
            if len(postlist[:beginning]) > 0:
                paging["before"] = postlist[beginning]
            if len(postlist[end:]) > 0:
                paging["after"] = postlist[end]
        current_app.logger.debug({"items": items, "paging": paging})
        return {"items": items, "paging": paging}

    def getvenue(self, hcard):
        """Get venue from the database."""
        card = json.loads(self.redis.hget("hcards", hcard[0].decode("utf-8")
                                          .split('/')[-1]).decode("utf-8"))
        geo = {
            "label": card["properties"]["name"][0],
            "latitude":
            card["properties"]["geo"][0]["properties"]["latitude"][0],
            "longitude":
            card["properties"]["geo"][0]["properties"]["longitude"][0],
            "url": hcard[0].decode("utf-8")
        }
        return geo

    def georadius(self, lat, lon):
        georadius = self.redis.georadius("venues", lon, lat, 100, "m", "asc")
        geo = georadius[0]
        return jsonify({
            "geo": self.getvenue(geo),
            "venues": [self.getvenue(venue) for venue in georadius]
        })
