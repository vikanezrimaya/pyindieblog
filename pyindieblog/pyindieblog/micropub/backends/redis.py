import json
import urllib
import redis
from flask import current_app, jsonify
from pyindieblog.micropub.backends.null import Database
from pyindieblog.micropub.helpers import getslug

class RedisDatabase(Database):
    """Redis-based store of posts."""

    def __init__(self, uri="", db=None):
        if db is not None:
            self.db = db
        else:
            self.db = redis.Redis(uri)

    def read_post(self, url):
        # Load and parse the JSON representation of a certain post in the database.
        # This clever line parses the URL path and gets out a location
        db = self.db
        key, post_id = urllib.parse.urlparse(url).path.strip('/').split('/')
        slug = db.hget("slugs", post_id)
        if slug:
            return json.loads(db.hget(key, slug))
        try:
            return json.loads(db.hget(key, post_id))
        except TypeError:
            raise FileNotFoundError

    # pylint: disable=inconsistent-return-statements
    def write_post(self, post, update=False):
        db = self.db
        if post["type"][0] == "h-card":
            # TODO h-card support
            if "mp-slug" in post["properties"]:
                post_id = post["properties"]["mp-slug"][0]
            else:
                post_id = post["properties"]["name"][0].replace(" ", "_").replace("/", "_")
            db.hset("hcards", post_id, json.dumps(post))
            if "geo" in post["properties"]:
                for g in post["properties"]["geo"]:
                    # Add it to the geoindex
                    db.geoadd("venues",
                            float(g["properties"]["longitude"][0]), float(g["properties"]["latitude"][0]),
                            f"https://{current_app.config['IDENTITY']}/hcards/{post_id}")
            return f"/hcards/{post_id}"
        # Generate a post slug
        post_id = getslug(post)
        if "url" in post["properties"]:
            key = urllib.parse.urlparse(post["properties"]["url"][0]).path.strip('/').split('/')[0]
        else:
            # Assume we're posting a post to a main feed here.
            key = "posts"
        # Write post JSON to the database
        db.hset(key, post_id, json.dumps(post))
        if "deleted" in post["properties"]:
            self.db.lrem("posts_order", 0, post_id)
        if not update:
            if key == "posts":
                # Push post into the main feed.
                db.lpush("posts_order", post_id)
                # Add the hashtags to the autocomplete list
                list(map(
                    lambda i: db.sadd("categories", i),
                    post["properties"].get("category", [])
                ))
                # Add slugs to the slugs array
                for slug in post["properties"].get("mp-slug", []):
                    db.hset("slugs", slug, post_id)

    def getvenue(self, hcard):
        geohcard = json.loads(self.db.hget("hcards", hcard[0].decode("utf-8").split('/')[-1]).decode("utf-8"))
        geo = {
            "label": geohcard["properties"]["name"][0],
            "latitude": geohcard["properties"]["geo"][0]["properties"]["latitude"][0],
            "longitude": geohcard["properties"]["geo"][0]["properties"]["longitude"][0],
            "url": hcard[0].decode("utf-8")
        }
        return geo

    def georadius(self, lat, lon):
        georadius = self.db.georadius("venues", lon, lat, 100, "m", "asc")
        geo = georadius[0]
        return jsonify({"geo": self.getvenue(geo), "venues": [self.getvenue(venue) for venue in georadius]})

    def post_list(self, limit=10, after=None, before=None, auth=None):
        # Load the list of post keys from Redis
        postlist = list(map(lambda i: i.decode('utf-8'), self.db.lrange("posts_order", 0, -1)))
        if after is not None:
            beginning = postlist.index(after)
            end = beginning + limit
        elif before is not None:
            end = postlist.index(before)
            beginning = end - limit
        else:
            beginning = 0
            end = limit
        if beginning < 0:
            beginning = 0
        # Apply the limits to get the current page
        filtered = postlist[beginning:end]
        if len(filtered) == 0:
            # If we have no posts, return just an empty list and no paging tokens.
            return {"items": []}
        # If we do have posts, get them from the database.
        # We could use read_post here, but db.hmget should be faster - because less network roundtrip, we're requesting everything in one go.
        items = list(map(json.loads, self.db.hmget("posts", filtered)))
        paging = {}
        if len(items) != len(postlist):
            if len(postlist[:beginning]):
                paging["before"] = postlist[beginning]
            if len(postlist[end:]):
                paging["after"] = postlist[end]
        current_app.logger.debug({"items": items, "paging": paging})
        return {"items": items, "paging": paging}
