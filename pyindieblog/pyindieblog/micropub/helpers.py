from datetime import datetime, timedelta, timezone
import dateutil

def get_timezone():
    return 0

def iso_time(time=None):
    """Return ISO 8601 time for a given time point or current time, if no time is specified."""
    return time.isoformat(timespec="seconds") if time is not None else datetime.now(tz=timezone(timedelta(hours=get_timezone()))).isoformat(timespec="seconds")

def getslug(post):
    # This should return a "canonical" slug for the DB.
    #if "mp-slug" in post["properties"]:
    #    return post["properties"]["mp-slug"]
    #else:
    return dateutil.parser.parse(post["properties"]["published"][0]).strftime("%s")

