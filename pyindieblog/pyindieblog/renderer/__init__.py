"""Front-end that connects to Micropub to render posts."""
# pylint: disable=invalid-name,line-too-long
import os
import sys
import json
import logging
import urllib
import itertools
import mf2py
import mf2util
import requests
import flask_theme
from flask import Flask, Blueprint, request, current_app, session, flash, redirect, _request_ctx_stack, url_for
from flask_theme import setup_themes, load_themes_from, render_theme_template, get_theme, packaged_themes_loader, theme_paths_loader, containable
from jinja2.exceptions import TemplateNotFound

DEFAULT_AVATAR = "https://upload.wikimedia.org/" \
                 + "wikipedia/commons/f/fd/Faenza-avatar-default-symbolic.svg"
renderer_blueprint = Blueprint('renderer', __name__)


class MicropubQueryException(Exception):
    """Exception that's thrown when Micropub doesn't respond correctly."""
    pass


def instance_themes_loader(app):
    """Load themes from instance folder in addition to packaged themes."""
    themes_dir = os.path.join(app.instance_path, 'themes')
    if os.path.isdir(themes_dir):
        return load_themes_from(themes_dir)
    return ()


def get_current_theme():
    """Get currently enabled theme for user."""
    theme = session.get('theme') or current_app.config.get("DEFAULT_THEME")
    try:
        return get_theme(theme)
    except KeyError:
        raise Exception("Tried to find theme named `{}', available themes: {}, current identifier: {}".format(theme, list(current_app.theme_manager.themes.values()), current_app.theme_manager.app_identifier))


def generate_theme_settings():
    """Generate HTML corresponding to themes in the theme selector."""
    string = ""
    for theme in current_app.theme_manager.list_themes():
        string += """<input type="radio"
          id="{themeid}
          name="theme"
          value="{themeid}"
          {checked}>
        <label for="{themeid}">
          {themename} by {themeauthor} - {themedescription}
        </label>\n""".format(
            themeid=theme.identifier,
            themename=theme.name,
            themedescription=theme.description,
            themeauthor=theme.author,
            checked=("checked" if get_current_theme() == theme.identifier else "")
        )
    return string


# This is a patch for flask-theme
def template_exists(templatename):
    """Check if a template exists for a theme.

    This is used to monkey_patch the Flask-Theme ext."""
    ctx = _request_ctx_stack.top
    return bytes(templatename, encoding="utf-8") in containable(ctx.app.jinja_env.list_templates())


flask_theme.template_exists = template_exists


def render_template(template, *args, **kwargs):
    """Render a site template according to current theme."""
    try:
        return render_theme_template(
            get_current_theme(), template, *args,
            settings=get_blog_settings(),
            templates=current_app.jinja_env.list_templates(),
            **kwargs)
    except TemplateNotFound as e:
        current_app.logger.error("TemplateNotFound. Path: " + repr(current_app.jinja_env.list_templates()))
        raise

def run():
    """Run the app. Used by the binary wrapper."""
    return get_app().run()


def get_app(testing=False):
    """Get an instance of the frontend application."""
    app = Flask(__name__)
    app.config.from_mapping(
        DEFAULT_THEME="plain"  # Plain theme, ships with the software.
        # Despite the name, it's fully functional and serves as a reference.
    )
    if app.debug:
        logging.basicConfig(level=logging.DEBUG)
        app.config.from_mapping(
            TESTING=testing,
            SECRET_KEY="dev",
            IDENTITY="fireburn.ru",
            MICROPUB_ENDPOINT="http://localhost:3000/",
            LEGACY_REDIRECT=True
        )
    else:
        app.config.from_envvar("PYINDIEBLOG_RENDERER_CONFIG")

    # Add in theme support!
    setup_themes(app, loaders=[
        packaged_themes_loader, theme_paths_loader, instance_themes_loader
    ])
    # Register a blueprint for the renderer
    app.register_blueprint(renderer_blueprint, url_prefix="/")
    # pylint: disable=no-member
    app.jinja_env.globals['hcardify'] = hcardify
    app.jinja_env.globals['venueify'] = venueify
    app.jinja_env.globals['hostname'] = hostname
    
    # pylint: disable=unused-variable
    if app.config.get("LEGACY_REDIRECT", False):
        @app.route("/note/<postid>")
        @app.route("/reply/<postid>")
        @app.route("/post/<postid>")
        @app.route("/photo/<postid>")
        @app.route("/like/<postid>")
        @app.route("/bookmark/<postid>")
        def legacy_redirect(postid):
            """Route that redirects everyone going by old links on old
            version of pyindieblog. #CoolURIsDontChange"""
            return redirect(f"/posts/{postid}", 301)
    return app


def get_blog_settings():
    """Get settings for the blog. They should be fetched from Micropub."""
    if current_app.debug or current_app.testing:
        # Return an array of static settings for testing.
        return {
            "blog": {
                "name": "Vika's Hideout",
                "subtitle": "A blog of a certain programmer girl",
                "footer": "&copy; Vika. Some rights reserved.",
                "author": {
                    "type": ["h-card"],
                    "properties": {
                        "name": ["Vika"],
                        "url": ["https://fireburn.ru/"],
                        "photo": [DEFAULT_AVATAR],
                        "pronoun": ["she/her"]
                    }
                }
            },
            "endpoints": {
                "micropub": current_app.config["MICROPUB_ENDPOINT"],
            }
        }

    if "ME" not in current_app.config:
        me = requests.get(
            current_app.config["MICROPUB_ENDPOINT"] + "/me",
            headers=({'Autorization': "Bearer {}".format(current_app.config["MICROPUB_ACCESS_TOKEN"])} if "MICROPUB_ACCESS_TOKEN" in current_app.config else {})
        ).json()
    else:
        me = current_app.config["ME"]
    return {
        "blog": me,
        "endpoints": {
            "authorization_endpoint": current_app.config.get("AUTHORIZATION_ENDPOINT"),
            "token_endpoint": current_app.config.get("TOKEN_ENDPOINT"),
            "micropub": current_app.config.get("MICROPUB_ENDPOINT"),
            "microsub": current_app.config.get("MICROSUB_ENDPOINT"),
            "webmention": current_app.config.get("WEBMENTION_ENDPOINT"),
            "micropub_media": current_app.config.get("MEDIA_ENDPOINT")
        }
    }

def query_micropub(**kwargs):
    """Send a GET request to the Micropub endpoint."""
    params = {'q': "source"}
    params.update({k: v for k, v in kwargs.items() if v is not None})
    current_app.logger.debug(params)
    if current_app.config.get("MICROPUB_ACCESS_TOKEN") == "":
        current_app.logger.error("MICROPUB_ACCESS_TOKEN is set to an empty string. Assuming bootstrapping phase. Your h-card shall be deployed from ME variable, making possible to use RelMeAuth for token bootstrapping.")
        return {"items": [{
            "type": ["h-entry"],
            "properties": {
                "published": [""],
                "content": ["I'm sorry, but posts are currently unavailable now.\n"
                            + "<br><b>Note to administrator</b>:\n"
                            + "you have probably set MICROPUB_ACCESS_TOKEN to "
                            + "an empty string. I do assume you have a reason "
                            + "for this - look into your log for details."],
                "url": ["#"]
            }
        }]}
    r = requests.get(
        current_app.config["MICROPUB_ENDPOINT"],
        params=params,
        # Only add Authorization: header if we have a token.
        # Not useful to not have a token except for debugging
        # In debug mode the endpoint doesn't check tokens AT ALL.
        headers=({'Authorization': "Bearer {}".format(
            current_app.config["MICROPUB_ACCESS_TOKEN"]
        )} if "MICROPUB_ACCESS_TOKEN" in current_app.config else {})
    )
    if r.status_code == 200:
        return r.json()
    elif r.status_code == 404:
        raise FileNotFoundError
    else:
        raise MicropubQueryException(
            "Error response from Micropub! "
            + "Sent {query_params}, got {response}".format(
                query_params=params, response=r
            )
        )


class CacheMissException(Exception):
    """Thrown to signify a cache miss."""
    pass


class Cache:
    """Simple cache."""
    # pylint: disable=unused-argument
    def __init__(self, name, default_ttl=64800):
        """Initialise a cache with a given name and default TTL."""
        self.items = {}

    def add(self, key, item, ttl=64800):
        """Add an item to cache."""
        self.items.update({key: item})
        return item

    def get(self, key, fallback=None):
        """Get an item from cache.

        Call fallback if it's callable, else throw a CacheMissException."""
        try:
            return self.items[key]
        except KeyError:
            if callable(fallback):
                return self.add(key, fallback(key))
            raise CacheMissException


def hostname(url):
    """Get hostname from URL."""
    return urllib.parse.urlparse(url).hostname


try:
    import uwsgi

    class UwsgiCache(Cache):
        """uwsgi-based cache."""
        # pylint: disable=super-init-not-called
        def __init__(self, name, default_ttl=64800):
            self.name = name
            self.ttl = default_ttl

        def add(self, key, item, ttl=None):
            if ttl is None:
                ttl = self.ttl
                uwsgi.cache_update(key, bytes(json.dumps(item), encoding="utf-8"), ttl, self.name)
            return item

        def get(self, key, fallback=None):
            if uwsgi.cache_exists(key, self.name):
                return json.loads(uwsgi.cache_get(key, self.name))
            if callable(fallback):
                return self.add(key, fallback(key))
            raise CacheMissException

    hcard_cache = UwsgiCache("h-card")

except ImportError:
    hcard_cache = Cache("h-card")


def gethcard(url):
    """Get an h-card representing a URL."""
    r = requests.get(url)
    print(url, r.url)
    page = mf2py.parse(doc=r.content, url=r.url)
    hcard = mf2util.representative_hcard(page, r.url)
    # or mf2util.representative_hcard(page, url + "/")
    return hcard


def hcardify(url):
    """Get an h-card for a URL, optionally fetching a cached one."""
    settings = get_blog_settings()
    if hostname(url) in list(
            map(hostname, settings["blog"]["author"]["properties"]["url"])
    ):
        return settings["blog"]["author"]
    return hcard_cache.get(url, fallback=gethcard)


# pylint: disable=inconsistent-return-statements
def venueify(checkin):
    """Fetch a venue for a checkin value."""
    if isinstance(checkin, str):
        if ";" in checkin:
            checkin = checkin.split(";")
            geo = checkin[0].split(":")[1].split(",")
            name = checkin[1].split("=")[1]
            url = checkin[2].split("=")[1]
            latitude = geo[0]
            longitude = geo[1]
            return {
                "name": name,
                "latitude": latitude,
                "longitude": longitude,
                "url": url
            }
        # It is an URL.
        try:
            if hostname(checkin) == hostname(request.url):
                j = query_micropub(url=checkin)
            else:
                # XXX Research how MF2 markup for h-geo works on others' sites
                # Right now we're requesting stuff from our own endpoint
                # and it gives us MF2 JSON already.
                # And I think it's pretty valid...
                # But other parsers may not give the same thing.
                # It may be either me deviating or the parser may not be ready
                # to parse experimental markup.
                # I need to consult with @aaronpk on this.
                httprequest = requests.get(checkin)
                j = list(filter(lambda h: "h-card" in h["type"], mf2py.parse(
                    doc=httprequest.content, url=httprequest.url
                )["items"]))[0]
            return {
                "name": j["properties"]["name"][0],
                "latitude": j["properties"]["geo"][0]["properties"]["latitude"][0],
                "longitude": j["properties"]["geo"][0]["properties"]["longitude"][0],
                "url": checkin
            }
        except:
            current_app.logger.error("Failed to parse h-card: %s", repr(j))
            return {
                "name": "Error: failed to parse venue. This was logged.",
                "url": checkin, "latitude": "0", "longtitude": "0"
            }
    elif isinstance(checkin, dict) and "h-card" in checkin["type"]:
        if "geo" in checkin["properties"]:
            lat = checkin["properties"]["geo"][0]["properties"]["latitude"][0]
            lon = checkin["properties"]["geo"][0]["properties"]["longitude"][0]
        else:
            lat = checkin["properties"]["latitude"][0]
            lon = checkin["properties"]["longitude"][0]
        return {
            "name": checkin["properties"]["name"][0],
            "latitude": lat,
            "longitude": lon,
            "url": checkin["properties"]["url"][0]
        }
    raise TypeError


def get_photo_url(photo):
    if isinstance(photo, dict):
        return urllib.parse.urlparse(photo["value"]).path
    else:
        return urllib.parse.urlparse(photo).path


@renderer_blueprint.route('/')
def index():
    """Home page view."""
    after = request.args.get("after")
    before = request.args.get("before")
    try:
        reply = query_micropub(before=before, after=after)
    except FileNotFoundError:
        return render_template(
            "static_page.html",
            post={
                "title": "404 Not Found",
                "content": "There is nothing here. Perhaps this was a mistake."
            }), 404
    else:
        return render_template(
            "main.html", posts=reply.get("items", []),
            paging=reply.get("paging", {}),
            partial_feed=True
        ), list(itertools.chain.from_iterable(
            [
                [("Link", "<{}>; rel=preload; as=image".format(get_photo_url(photo)))
                 # pylint: disable=unsubscriptable-object
                 for photo in post["properties"].get("photo", [])]
                for post in reply.get("items", [])
            ]))

@renderer_blueprint.route("/feed")
def full_feed():
    """Home page view for indie reader consumption."""
    after = request.args.get("after")
    before = request.args.get("before")
    try:
        reply = query_micropub(before=before, after=after)
    except FileNotFoundError:
        return render_template(
            "static_page.html",
            post={
                "title": "404 Not Found",
                "content": "There is nothing here. Perhaps this was a mistake."
            }), 404
    else:
        return render_template(
            "main.html", posts=reply.get("items", []),
            paging=reply.get("paging", {}),
            partial_feed=False
        ), #list(itertools.chain.from_iterable(
           # [
           #     [("Link", "<{}>; rel=preload; as=image".format(urllib.parse.urlparse(photo).path))
           #      # pylint: disable=unsubscriptable-object
           #      for photo in post["properties"].get("photo", [])]
           #     for post in reply.get("items", [])
           # ]))


# Tag feeds need a query filter syntax and I still haven't decided on one :3
# @renderer_blueprint.route('/tags/<tag>')
# def tagfeed(tag):
#     posts = []
#     return render_template("feed.html", posts=posts)


# pylint: disable=unused-argument
@renderer_blueprint.route('/hcards/<cardid>')
def hcard(cardid):
    """View for h-card."""
    hcard = query_micropub(url=request.base_url)
    return render_template("hcard.html", hcard=hcard)


# pylint: disable=unused-argument
@renderer_blueprint.route('/posts/<postid>')
def post(postid):
    """View for post (h-entry)."""
    hentry = query_micropub(url=request.base_url)
    if "deleted" in hentry["properties"]:
        return render_template("post.html", post=hentry), 410
    return render_template("post.html", post=hentry), [
        ("Link", "<{}>; rel=preload; as=image".format(get_photo_url(photo)))
        for photo in hentry["properties"].get("photo", [])
    ]


# pylint: disable=unused-argument
@renderer_blueprint.route('/pages/<pageid>')
def page(pageid):
    """View for a static page."""
    page_content = query_micropub(url=request.base_url)
    return render_template("static_page.html", post=page_content)


@renderer_blueprint.route('/settings', methods=["GET", "POST"])
def settings_view():
    """Settings page that allows to change themes."""
    if request.form:
        session['theme'] = request.form.get("theme")
        flash("Settings updated", "ok")
        return redirect(url_for(".settings_view"))

    pagedata = {"title": "Settings", "content": """
    <form id="globalsettings" method="POST">
        <fieldset id="themesettings">
             <legend>Theme settings</legend>
             {themes}
        </fieldset>
        <input type="submit" value="Save and apply">
    </form>
    """.format(themes=generate_theme_settings())}
    return render_template("static_page.html", **pagedata)
