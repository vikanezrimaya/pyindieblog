"""Unit tests that act as sanity checks for Micropub."""
# import os
# import sys
import tempfile
import json
import unittest
import shutil
from unittest.mock import patch
import fakeredis
import pyindieblog
from pyindieblog.micropub import get_app
from pyindieblog.micropub.backends import RedisDatabase

# MOCKCOUCHDBURL = "http://couchdb.example.com"
# class MockJSONResponse:
#     def __init__(self, status, json):
#         self.status_code = status
#         self._json = json

#     def json(self):
#         return self._json

# class CouchDBMocks:
#     @staticmethod
#     def mock_empty_db_get(url, params=None, **kwargs):
#         if "_find" in url:
#             return MockJSONResponse(200, {"docs": [], "bookmark": "nil"})
#         else:
#             # Not implementing
#             return MockJSONResponse(501, {"awoo": "awoooooo~"})

#     @staticmethod
#     def mock_insertion_get(url, params=None, **kwargs):
#         pass

# class MicropubWithCouchDBTestCase(unittest.TestCase):
#     def setUp(self):
#         app = get_app(CouchDatabase(MOCKCOUCHDBURL))
#         app.testing = True
#         self.client = app.test_client()

#     @mock.patch('pyindieblog.micropub.backends.couchdb.requests.post',
#                 side_effect=CouchDBMocks.mock_empty_db_get)
#     def test_empty_post_db(self, mock_find):
#         """Test that Micropub output is valid even with an empty database."""
#         rv = self.client.get('/?q=source')
#         self.assertIn(rv.status, ["200 OK"])
#         # Ensure that no posts are returned but the JSON is valid.
#         json_out = rv.get_json()
#         self.assertEqual(len(json_out["items"]), 0)


class MicropubWithRedisTestCase(unittest.TestCase):
    """Test suite for Redis backend, which is supposed to be fast."""

    def setUp(self):
        fakedb = fakeredis.FakeRedis()
        app = get_app(RedisDatabase(db=fakedb))
        app.testing = True
        self.client = app.test_client()
        app.config["DATABASE"].db.flushall()  # Flush the test database

    def test_empty_post_db(self):
        """Test source query for post list with an empty database."""
        request = self.client.get('/?q=source')
        self.assertIn(request.status, ["200 OK"])
        # Ensure that no posts are returned but the JSON is valid.
        json_out = request.get_json()
        self.assertEqual(len(json_out["items"]), 0)

    def test_insert_post_form(self):
        """Test form-encoded input."""
        rv1 = self.client.post('/', data={
            "h": "entry", "content": "Test post for Micropub."
        })
        self.assertIn(rv1.status, ["201 CREATED", "202 ACCEPTED"])
        rv2 = self.client.get(f'/?q=source&url={rv1.headers["Location"]}')
        self.assertIsNotNone(rv2.get_json())

    def test_insert_post_json(self):
        """Test JSON input."""
        rv1 = self.client.post('/', data=json.dumps({
            "type": ["h-entry"],
            "properties": {
                "content": ["Test post for Micropub, passed as JSON."]
            }
        }), content_type="application/json")
        self.assertIn(rv1.status, ["201 CREATED", "202 ACCEPTED"])
        rv2 = self.client.get(f'/?q=source&url={rv1.headers["Location"]}')
        self.assertIsNotNone(rv2.get_json())

    # def test_properties_query(self):
    #     """Test that a server can return single properties."""
    #

    def test_post_with_timestamp(self):
        """Test that timestamps in the past are not overwritten."""
        time = "2019-08-08T22:38:36-07:00"  # Just a random time
        rv1 = self.client.post('/', data=json.dumps({
            "type": ["h-entry"], "properties": {
                "content": "Test post for Micropub.\n"
                           + "Passed as JSON with the timestamp in the past.",
                "published": [time]
            }
        }), content_type="application/json")
        self.assertIn(rv1.status, ["201 CREATED", "202 ACCEPTED"])
        rv2 = self.client.get(f'/?q=source&url={rv1.headers["Location"]}')
        j = rv2.get_json()
        # Assert that it parsed correctly
        self.assertIsNotNone(j)
        # Timestamp should be intact.
        self.assertEqual(
            j["properties"]["published"][0], time
        )

    def test_post_list(self):
        """Insert a post and then ensure it shows in the main feed."""
        rv1 = self.client.post('/', data=json.dumps({
            "type": ["h-entry"],
            "properties": {
                "content": ["Test post for Micropub, passed as JSON."]
            }
        }), content_type="application/json")
        self.assertIn(rv1.status, ["201 CREATED", "202 ACCEPTED"])
        rv2 = self.client.get('/?q=source')
        self.assertIn(rv2.status, ["200 OK"])
        json_out = rv2.get_json()
        self.assertEqual(len(json_out["items"]), 1)

    def test_404(self):
        """Test that unknown posts get 404."""
        rv1 = self.client.get('/?q=source&url=https://fireburn.ru/posts/awoo')
        self.assertEqual(rv1.status, "404 NOT FOUND")

    def test_delete(self):
        """Test that deleted posts stay in the database, but with dt-deleted.
        It is a marker that post is deleted, and allows later undeletion.

        Undeletion is also tested here."""
        rv1 = self.client.post('/', data=json.dumps({
            "type": ["h-entry"],
            "properties": {
                "content": ["Test post for Micropub, passed as JSON."
                            + "It will be deleted and undeleted."]
            }
        }), content_type="application/json")
        self.assertIn(rv1.status, ["201 CREATED", "202 ACCEPTED"])
        rv2 = self.client.get(f'/?q=source&url={rv1.headers["Location"]}')
        j = rv2.get_json()
        self.assertEqual(j["type"][0], "h-entry")
        rvdel = self.client.post('/', data=json.dumps({
            "action": "delete",
            "url": rv1.headers["Location"]
        }), content_type="application/json")
        self.assertEqual(rvdel.status, "200 OK")
        rv3 = self.client.get(f'/?q=source&url={rv1.headers["Location"]}')
        j = rv3.get_json()
        self.assertEqual(len(j["properties"]["deleted"]), 1)
        rvundel = self.client.post('/', data=json.dumps({
            "action": "undelete",
            "url": rv1.headers["Location"]
        }), content_type="application/json")
        self.assertEqual(rvundel.status, "200 OK")
        rv4 = self.client.get(f'/?q=source&url={rv1.headers["Location"]}')
        j = rv4.get_json()
        self.assertEqual(len(j["properties"].get("deleted", [])), 0)

    def test_hcard(self):
        """Insert a venue h-card, and then ensure that it's fetchable."""
        rv1 = self.client.post('/', data=json.dumps({
            "type": ["h-card"],
            "properties": {
                "mp-slug": ["anticafe_krilya"],
                "name": ["Антикафе Крылья"],
                "url": [
                    "https://vk.com/anticafe_krilya",
                    "https://instagram.com/anticafe_krilya"
                ]
            }
        }), content_type="application/json")
        self.assertIn(rv1.status, ["201 CREATED", "202 ACCEPTED"])
        # Ensure that it got posted to h-cards
        self.assertIn("hcards", rv1.headers["Location"])
        rv2 = self.client.get(f'/?q=source&url={rv1.headers["Location"]}')
        self.assertIn(rv2.status, ["200 OK"])
        json_out = rv2.get_json()
        self.assertEqual(json_out["type"][0], "h-card")

    @unittest.skip("FakeRedis does not support geoqueries.")
    def test_geoquery(self):
        """Ensure that geoqueries are able to find your current location."""
        lat = "53.20090"
        lon = "45.01890"
        rv1 = self.client.post('/', data=json.dumps({
            "properties": {
                "mp-slug": ["anticafe_krilya"],
                "name": ["Антикафе Крылья"],
                "url": [
                    "https://vk.com/anticafe_krilya",
                    "https://instagram.com/anticafe_krilya"
                ],
                "geo": [{
                    "type": ["h-geo"],
                    "properties": {
                        "latitude": ["53.20090"],
                        "longitude": ["45.01890"]
                    }
                }],
            },
            "type": [
                "h-card"
            ]
        }), content_type="application/json")
        self.assertIn(rv1.status, ["201 CREATED", "202 ACCEPTED"])
        # Ensure that it got posted to h-cards
        self.assertIn("hcards", rv1.headers["Location"])
        rv2 = self.client.get("/?q=geo&lat={}&lon={}".format(lat, lon))
        json_out = rv2.get_json()
        self.assertIn("geo", json_out)

    def test_markdown_conversion(self):
        """Ensure that markdown gets converted to HTML."""
        rv1 = self.client.post('/', data=json.dumps({
            "type": ["h-entry"],
            "properties": {
                "content": ["This is a **post** that should be converted to [Markdown](https://daringfireball.net/projects/markdown)."],
            }
        }), content_type="application/json")
        self.assertIn(rv1.status, ["201 CREATED", "202 ACCEPTED"])
        rv2 = self.client.get(f'/?q=source&url={rv1.headers["Location"]}')
        json_out = rv2.get_json()
        self.assertIsInstance(json_out["properties"]["content"][0], dict)
        self.assertIn("html", json_out["properties"]["content"][0])
        self.assertIn("value", json_out["properties"]["content"][0])

class MicropubWithFileRedisTestCase(MicropubWithRedisTestCase):
    """Test suite for file-based backend with Redis caching,
    which is supposed to be both fast and resillient to failure."""

    def setUp(self):
        self.fakedb = fakeredis.FakeRedis()
        self.tmp = tempfile.mkdtemp()
        app = get_app(RedisDatabase(db=self.fakedb))
        app.testing = True
        self.client = app.test_client()

    def tearDown(self):
        shutil.rmtree(self.tmp)
        self.fakedb.flushall()  # Flush the test database
