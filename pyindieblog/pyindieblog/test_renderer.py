"""Unit tests that act as sanity checks for the frontend."""
from datetime import datetime
import unittest
from unittest.mock import patch
import mf2py
import pyindieblog.renderer
SIMPLE_NOTE_CONTENT="This is a simple note."
SIMPLE_ARTICLE_CONTENT="This is an article. It is really long. Lorem ipsum dolor sit amet..."
SIMPLE_ARTICLE_SUMMARY="This is a summary of the article."

simple_note = {
    "type": ["h-entry"],
    "properties": {
        "url": ["https://fireburn.ru/post/10000"],
        "content": [SIMPLE_NOTE_CONTENT],
        # It's not crucially important which time we use.
        "published": [datetime.now().isoformat(timespec="seconds")]
    }
}

simple_article = {
    "type": ["h-entry"],
    "properties": {
        "url": ["https://fireburn.ru/post/20000"],
        "content": [SIMPLE_ARTICLE_CONTENT],
        "summary": [SIMPLE_ARTICLE_SUMMARY],
        "published": [datetime.now().isoformat(timespec="seconds")]
    }
}

simple_checkin = {
    "type": ["h-entry"],
    "properties": {
        "url": ["https://fireburn.ru/post/40000"],
        "content": [SIMPLE_NOTE_CONTENT],
        "published": [datetime.now().isoformat(timespec="seconds")],
        "checkin": ["https://fireburn.ru/hcards/venue"],
    }
}

simple_venue = {
    "type": ["h-card"],
    "properties": {
        "url": ["https://fireburn.ru/hcards/venue", "https://vk.com/anticafe_krilya", "https://instagram.com/anticafe_krilya"],
        "name": "Антикафе Крылья",
        "geo": [{
            "type": ["h-geo"],
            "properties": {
                "latitude": ["45.0"],
                "longitude": ["45.0"]
            }
        }]
    }
}

class RendererTestCase(unittest.TestCase):
    def setUp(self):
        app = pyindieblog.renderer.get_app(testing=True)
        app.testing = True
        self.client = app.test_client()

    @patch("pyindieblog.renderer.query_micropub", return_value={"items": []})
    def test_empty_post_db(self, query_micropub):
        """Test that pyindieblog does not bail out on an empty database
        and a valid h-card is generated."""
        request = self.client.get('/')
        self.assertIn(request.status, ["200 OK"])
        parsed = mf2py.parse(doc=request.data)
        self.assertEqual(parsed["items"][0]["type"][0], "h-card")
        self.assertEqual(parsed["items"][1]["type"][0], "h-feed")

    @patch("pyindieblog.renderer.query_micropub", side_effect=[{"items": [simple_note]}])
    def test_one_post_db(self, query_micropub):
        """Test that one post can be successfully processed."""
        request = self.client.get('/')
        self.assertIn(request.status, ["200 OK"])
        parsed = mf2py.parse(doc=request.data)
        self.assertEqual(parsed["items"][1]["type"][0], "h-feed")
        self.assertEqual(parsed["items"][1]["children"][0]["properties"]["content"][0]["html"], SIMPLE_NOTE_CONTENT)

    @patch("pyindieblog.renderer.query_micropub", side_effect=({"items": [simple_article]}, simple_article))
    def test_article_summary(self, query_micropub):
        request = self.client.get('/')
        self.assertIn(request.status, ["200 OK"])
        parsed = mf2py.parse(doc=request.data)
        self.assertEqual(parsed["items"][1]["type"][0], "h-feed")
        self.assertEqual(parsed["items"][1]["children"][0]["properties"]["summary"][0], SIMPLE_ARTICLE_SUMMARY)
        post_request = self.client.get('/posts/20000')
        self.assertIn(post_request.status, ["200 OK"])
        parsed_post = mf2py.parse(doc=post_request.data)
        print(parsed_post)
        self.assertEqual(parsed_post["items"][0]["type"][0], "h-entry")
        self.assertEqual(parsed_post["items"][0]["properties"]["content"][0]["html"], SIMPLE_ARTICLE_CONTENT)

    @patch("pyindieblog.renderer.query_micropub", side_effect=({"items": [simple_checkin]}, simple_venue))
    def test_checkin(self, query_micropub):
        request = self.client.get("/", base_url="https://fireburn.ru/")
        self.assertIn(request.status, ["200 OK"])
        parsed = mf2py.parse(doc=request.data)
        print(parsed)
        self.assertEqual(parsed["items"][1]["type"][0], "h-feed")
        self.assertIn("checkin", parsed["items"][1]["children"][0]["properties"])
