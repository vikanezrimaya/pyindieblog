"""A common library for addressing various microservices."""
import requests
import json
import mf2py
import hashlib
import time
from flask import current_app
try:
    import fakeredis
except ImportError:
    pass

def is_test_db():
    try:
        if isinstance(current_app.config["DATABASE"], fakeredis.FakeRedis):
            return True
    except:
        pass
    return False

# XXX provide something like a default endpoint URL? :c
def validate_token(token, endpoint=None):
    """Validate an IndieAuth token, using a provided endpoint. Return validity of the token and all information from the endpoint.

    Please note that scope parameter is automatically converted to a set for your convenience.

    Please also note that in case of ANY failure this function will failsafe to saying that token is INVALID.
    
    Please ALSO note that in DEBUG mode this application WILL ACCEPT ANY TOKEN!!!"""
    if current_app.debug or current_app.testing or is_test_db():
        return True, {"me": "https://fireburn.ru/", "scope": {"create", "update", "media", "pyindieblog:posts_db", "delete"}}
    try:
        r = requests.get(endpoint, headers={"Authorization": "Bearer {}".format(token), "Accept": "application/json"})
        j = r.json()
        if "error" in j: # something went wrong
            return False, j
        j["scope"] = set(j["scope"].split(' '))
        return True, j
    except: # something went TERRIBLY WRONG
        return False, {}

def hash_for_vouch(source):
    t = int(time.time())
    counter = 0
    while True:
        dig = hashlib.sha256("%s-%s-%s" % (url, t, counter)).hexdigest()
        if dig.startswith("00000"):
            r = requests.post('https://hash-for-vouch.herokuapp.com/endpoint', data={"time": t, "nonce": counter, "source": source})
            if r.status_code == 200:
                return r.json()["url"]
        counter += 1

def send_webmention(source, target, vouch=None):
    """Send a webmention to the target about the source.

    This is a multi-step algorithm, consisting of:
    1. Finding a Webmention endpoint (usually rel=webmention)
    2. (optional) Finding a suitable Vouch URL (currently not implemented) - this allows to circumvent moderation in some cases
    3. Sending a form-encoded POST with source, target (and optionally vouch) specified.

    We don't use a callback here."""
    try:
        endpoint = mf2py.parse(url=target)["rels"]["webmention"][0]
    except KeyError:
        pass
    else:
        params = {
            "source": source,
            "target": target
        }
        if vouch is not None:
            params["vouch"] = vouch
        if "brid.gy" in target:
            params["bridgy_omit_link"] = "maybe"
        r = requests.post(endpoint, data=params)
        if r.status_code == 201:
            if "Location" in r.headers:
                # Syndicators return the syndicated resource (at least brid.gy and IndieNews)
                # Some webmention processors return a status URL
                return r.headers["Location"]
        elif r.status_code == 449:
            # This endpoint supports Vouch.
            # Let's hope they accept some proof-of-work!
            return send_webmention(source, target, vouch=hash_for_vouch(source))
        elif r.status_code == 400:
            # Our webmention was rejected.
            if "vouch" in params:
                current_app.logger.error("Error sending webmention from %s to %s. Used %s as vouch, we were rejected.", source, target, vouch)
            else:
                current_app.logger.error("Error sending webmention from %s to %s.", source, target)
