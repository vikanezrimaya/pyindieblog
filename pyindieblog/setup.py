from setuptools import setup, find_packages

print(find_packages())

setup(
    name='pyindieblog',
    version='0.1.0',
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=['flask', 'mf2py', 'requests', 'redis', 'python-dateutil', 'mf2util', 'Flask-Theme==0.2.0', 'markdown'],
    tests_require=['fakeredis'],
    entry_points={
        'console_scripts': [
            'pyindieblog-micropub=pyindieblog.micropub:run',
            'pyindieblog-renderer=pyindieblog.renderer:run'
        ]
    }
)
