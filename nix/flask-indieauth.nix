{ python3Packages, fetchPypi }:
python3Packages.buildPythonPackage rec {
  pname = "Flask-IndieAuth";
  version = "0.0.3.2";
  src = fetchPypi {
    inherit pname version;
    sha256 = "1kzciwibyqd0hsky4c274lf9pm0zxxn3g7vgdafd417qwzrf6qdr";
  };
  propagatedBuildInputs = with python3Packages; [
    flask
  ];
}
