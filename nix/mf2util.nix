{ stdenv, buildPythonPackage, fetchPypi, pytest, mf2py }:
buildPythonPackage rec {
  pname = "mf2util";
  version = "0.5.1";
  src = fetchPypi {
    inherit pname version;
    sha256 = "0nf8cx1d2lkfrvz4wdl8jn2mf7m4yq7hdmkcnhf9pqaksn6y1dbh";
  };
  checkInputs = [ pytest mf2py ];

  doCheck = false; # XXX broken
  meta = with stdenv.lib; {
    description = "Utilities for interpreting microformats2 data";
  };
}
