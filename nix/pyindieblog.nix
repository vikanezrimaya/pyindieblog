{ stdenv, python3Packages }:
python3Packages.buildPythonPackage rec {
  pname = "pyindieblog";
  version = "0.1.0";
  src = ../pyindieblog;
  sourceRoot = "pyindieblog";

  propagatedBuildInputs = with python3Packages; [
    flask flask-theme redis requests mf2py mf2util dateutil markdown
  ];
  checkInputs = with python3Packages; [
    fakeredis
  ];

  checkPhase = ''
    FLASK_ENV=development FLASK_DEBUG=true python setup.py test
  '';

  shellHook = ''
    PATH=${python3Packages.python-language-server}/bin:$PATH
  '';

  meta = with stdenv.lib; {
    homepage = "https://git.fireburn.ru/projects/pyindieblog";
    description = "A central server for an IndieWeb-enabled blog";
    license = licenses.mit;
  };
}

