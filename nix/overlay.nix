let
  pythonOverlay = pyself: pysuper: {  
    mf2py = pysuper.callPackage ./mf2py.nix {};
    mf2util = pysuper.callPackage ./mf2util.nix {};
    flask-theme = pysuper.callPackage ./flask-theme.nix {};
    fakeredis = pysuper.callPackage ./fakeredis.nix {};
    pyindieblog = pysuper.callPackage ./pyindieblog.nix {};
    spano = pysuper.callPackage ./spano.nix {};
    flask-indieauth = pysuper.callPackage ./flask-indieauth.nix {};
    flask-hashfs = pysuper.callPackage ./flask-hashfs.nix {};
    hashfs = pysuper.callPackage ./hashfs.nix {};
  };
in
final: prev: {
  python3 = prev.python3.override {
    packageOverrides = pythonOverlay;
  };
  pyindieblog = prev.python3Packages.toPythonApplication prev.python3Packages.pyindieblog;
}
