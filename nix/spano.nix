{ python3Packages, fetchFromGitHub }:
python3Packages.buildPythonPackage rec {
  pname = "spano";
  version = "1.0";
  src = fetchFromGitHub {
    owner = "kisik21";
    repo = pname;
    rev = "12788898ac95a0b01d58bfc320bd0124a582847a";
    sha256 = "0rbpwpqiqr96bgnhnk9vrylvqlkb736pwax3lf4vw8qhxny8hdfs";
  };
  propagatedBuildInputs = with python3Packages; [
    flask flask-indieauth flask-hashfs python_magic
  ];
}
