{ stdenv, buildPythonPackage, fetchPypi, flask, nose }:
buildPythonPackage rec {
  pname = "flask-theme";
  version = "0.2.0";
  src = fetchPypi {
    pname = "Flask-Theme";
    inherit version;
    sha256 = "1j6k133hvidjb81wbgw80bkvdbhyyf03j9m0w5900n451lgz415m";
  };
  propagatedBuildInputs = [ flask ];
  propagatedCheckInputs = [ nose ];
  doCheck = false; # XXX tests
  meta = with stdenv.lib; {
    description = "Provides infrastructure for theming Flask applications";
  };
}
