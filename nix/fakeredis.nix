{ stdenv, buildPythonPackage, fetchPypi, six, sortedcontainers, redis }:
buildPythonPackage rec {
  pname = "fakeredis";
  version = "1.0.4";

  src = fetchPypi {
    inherit pname version;
    sha256 = "1n4yilsdwmmfnv2qfpc7h08nhf0xx07ck9ca9r8k12g79p1jvfrs";
  };

  propagatedBuildInputs = [
    six sortedcontainers redis
  ];

  doCheck = false; # XXX tests
  
  meta = with stdenv.lib; {
    description = "Fake implementation of redis API (redis-py) for testing purposes";
    homepage = "https://github.com/jamesls/fakeredis";
  };
}
