{ python3Packages, fetchPypi }:
python3Packages.buildPythonPackage rec {
  pname = "Flask-HashFS";
  version = "0.3.0";
  src = fetchPypi {
    inherit pname version;
    sha256 = "1zcw3n8gq529m1bnszclkj0nrk5hrnpnn3jcjmgqkd85y3p4nla7";
  };
  propagatedBuildInputs = with python3Packages; [
    flask
    hashfs
  ];
  checkInputs = with python3Packages; [ tox ];
  doCheck = false;
}
