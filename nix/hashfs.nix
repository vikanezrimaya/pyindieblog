{ python3Packages, fetchPypi }:
python3Packages.buildPythonPackage rec {
  pname = "hashfs";
  version = "0.7.1";
  src = fetchPypi {
    inherit pname version;
    sha256 = "1cyswdds15bavnwnpx86hxfwxg2wj869n0kdfw78v03fj08g7rqg";
  };
  doCheck = false;
}
